
package inventory;

import java.util.Scanner;

/**
 *
 * @author Urwah Khan
 */

        class Item {

    public int itemID;
    private String name;
    private int quantity;

    
    public static Item[] inventory = new Item[100];
    private int itemCounter = 0; 

    
    public Item(int itemID, int quantity, String name) {
        this.itemID = itemID;
        this.quantity = quantity;
        this.name = name;
    }
    public int getItemId(){
        return this.itemID;
    }
    public int getQuantity(){
        return this.quantity;
    }
    public String getName(){
        return this.name;
    }

    
    public void addItem(Item newItem) {
        inventory[itemCounter] = newItem;
        itemCounter++;
        quantity++;
    }

    
    public void printInventory() {
        for (int i = 0; i < itemCounter; i++) {
            System.out.println("ID: " + inventory[i].itemID
                    + "\t Name: " + inventory[i].name
                    + "\t Quantity:" + inventory[i].quantity);
        }
    }

   
    public int getItemQuantity(int ID) {
        int temp = 0;
        for (int j = 0; j < itemCounter; j++) {
            if (inventory[j].itemID == ID) {
                temp = inventory[j].quantity;
                break;
            }
        }
        return temp;
    }
    

}

public class  Inventory  {
  
public static void main(String[]args){
        
        Item item1 = new Item(0, 0, null);

      
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter name of the item to add:");
        String name = input.nextLine();
        
        System.out.println("Enter the quantity:");
        int quantity = input.nextInt();
        
        Item item2 = new Item(++item1.itemID, quantity, name);
        item1.addItem(item2);

        Item item3 = new Item(++item1.itemID, 5, "ET-2750 printer");
        item1.addItem(item3);

        Item item4 = new Item(++item1.itemID, 10, "MX-34 laptops");
        item1.addItem(item4);
        
        
        Item item5 = new Item(++item1.itemID, 77, "IPhone 12 Pro"); 
        Item item6 = new Item(++item1.itemID, 11,"Huawei Matebook D15");
       
        item1.printInventory();

        System.out.println("Enter ID of the item whose quantity you want to find:");
        int temp_ID = input.nextInt();
        System.out.println("Item's quantity is: " + item1.getItemQuantity(temp_ID));

    }
}
        
   

